<?php

it('has country page with valid API_KEY', function () {
    $response = $this->json('GET', 'api/list-countries', [], ['API_KEY' => config('app.API_KEY')]);
    $response->assertStatus(200);
});

it('has country detail page with valid API_KEY', function () {
    $response = $this->json('GET', 'api/list-countries/pt', [], ['API_KEY' => config('app.API_KEY')]);
    $response->assertStatus(200);
});

it('has country page with invalid API_KEY', function () {
    $response = $this->json('GET', 'api/list-countries', [], ['API_KEY' => 'asdasd']);
    $response->assertStatus(403);
});

it('has country detail page with invalid API_KEY', function () {
    $response = $this->json('GET', 'api/list-countries/pt', [], ['API_KEY' => 'asdasd']);
    $response->assertStatus(403);
});

it('has country page without API_KEY', function () {
    $response = $this->json('GET', 'api/list-countries');
    $response->assertStatus(403);
});

it('has country detail page without API_KEY', function () {
    $response = $this->json('GET', 'api/list-countries/pt');
    $response->assertStatus(403);
});
