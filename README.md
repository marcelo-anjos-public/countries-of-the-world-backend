# RakeTech Countries - Backend - Code Challenge

This project intends to demonstrate my code skills.

## Requirements
- [Docker Desktop](https://www.docker.com/products/docker-desktop)
- [Composer (PHP Dependency Manager)](https://getcomposer.org/)

## Installation

1. Clone the repository
2. Create `.env` file in the root of the project (check `.env.example` for reference):
3. Run `composer install` to install the dependencies
4. Add the following environment variables to the `.env` file:
```env
API_KEY="<YOUR_API_KEY>" #Note this API key should be the same as the one defined on the frontend project on api_config.json
```
5. Run `./vendor/bin/sail up` to start the development server
6. Open your browser and go to `http://localhost`

## Testing
To run the tests, execute the following command:
```sail artisan test```

To access the request directly from POSTMAN, you can use the following collection:
```url
http://localhost/api/list-countries 
with API_KEY parameter as a header (must be same defined on 4.)
```

> [!WARNING]
> Not intended for production, and it is not production ready.
