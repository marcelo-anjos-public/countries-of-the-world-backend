<?php

namespace App\Repository;

use Illuminate\Support\Collection;

interface CountryRepositoryInterface
{
    public function all(): Collection;

    public function find(string $alpha): ?array;
}
