<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;

interface RestCountriesRepositoryInterface
{
    /**
     * @param $id
     * @return Model
     */
    public function find($id): ?Model;
}
