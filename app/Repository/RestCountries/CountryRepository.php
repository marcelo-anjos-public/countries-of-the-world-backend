<?php

namespace App\Repository\RestCountries;

use App\Repository\CountryRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class CountryRepository implements CountryRepositoryInterface
{
    /**
     * @param $id
     * @return array
     */
    public function find(string $alpha): ?array
    {
        $data = $this->fetchFromProvider();

        return $data->where('cca2', $alpha)->first();
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->fetchFromProvider()->sortBy('name.official', SORT_NATURAL);
    }

    /**
     * @return Collection
     */
    private function fetchFromProvider(): Collection
    {
        $response = Cache::get('countries', function () {
            $response = Http::get('https://restcountries.com/v3.1/all');
            Cache::put('countries', $response->json(), now()->addHour());
            return $response->json();
        });


        return collect($response);
    }
}
