<?php

namespace App\Http\Controllers;

use App\Http\Resources\CountryResource;
use App\Http\Resources\CountryResourceCollection;
use App\Repository\CountryRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class CountryController extends Controller
{

    /**
     * @param CountryRepositoryInterface $countryRepository
     */
    public function __construct(private readonly CountryRepositoryInterface $countryRepository)
    {
    }

    /**
     * @return CountryResourceCollection
     */
    public function index(): CountryResourceCollection
    {
        return new CountryResourceCollection($this->countryRepository->all());
    }

    /**
     * @param string $alpha
     * @return CountryResource
     */
    public function find(string $alpha): CountryResource
    {
        return new CountryResource($this->countryRepository->find(strtoupper($alpha)));
    }
}
