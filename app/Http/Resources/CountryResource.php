<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CountryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'official_name' => $this->resource['name']['official'],
            'flag' => $this->resource['flags']['svg'],
            'map' => $this->resource['maps']['openStreetMaps'],
        ];
    }
}
