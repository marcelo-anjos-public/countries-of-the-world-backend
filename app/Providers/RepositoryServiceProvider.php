<?php

namespace App\Providers;

use App\Repository\CountryRepositoryInterface;
use App\Repository\RestCountries\BaseRepository;
use App\Repository\RestCountries\CountryRepository;
use App\Repository\RestCountriesRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(RestCountriesRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(CountryRepositoryInterface::class, CountryRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
